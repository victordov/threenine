package demo;

import demo.dto.TreeNine;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
@RestController
@EnableScheduling
public class DemoApplication {


    @Autowired
    private SimpMessagingTemplate webSocketMessage;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    @RequestMapping(value = "/api/first-page")
    public ResponseEntity<?> sayHello() throws IOException {

        return new ResponseEntity<>(getDataFrom999(), HttpStatus.OK);
    }

    private List<TreeNine> getDataFrom999() throws IOException {
        Document doc = Jsoup.connect("https://999.md").get();
        Elements select = doc.select(".addItem");
        List<TreeNine> list = new ArrayList<>();
        for (Element one : select) {
            TreeNine treeNine = new TreeNine();
            String photo = one.getElementsByClass("photo").attr("data-image");
            if (!photo.isEmpty()) {
                treeNine.setPhoto("http://" + photo.substring(2));
            }
            treeNine.setId("http://999.md/" + one.getElementsByClass("title").get(0).getElementsByTag("a").attr("href"));
            treeNine.setTitle(one.getElementsByClass("title").text());
            list.add(treeNine);
        }
        return list;
    }

    private List<TreeNine> getDataFrom999Date() throws IOException {
        List<TreeNine> list = new ArrayList<>();
        long time = new Date().getTime() / 1000 - 60;
        Document doc = Jsoup.connect("https://999.md/last?from=" + time).get();
        Elements select = doc.select(".addItem");

        for (Element one : select) {
            TreeNine treeNine = new TreeNine();
            String photo = one.getElementsByClass("photo").attr("data-image");
            if (!photo.isEmpty()) {
                treeNine.setPhoto("http://" + photo.substring(2));
            }
            treeNine.setId("http://999.md/" + one.getElementsByClass("title").get(0).getElementsByTag("a").attr("href"));
            treeNine.setTitle(one.getElementsByClass("title").text());
            list.add(treeNine);
        }
        return list;
    }

    @Scheduled(fixedRate = 10000)
    public void reportCurrentTime() throws IOException {
        webSocketMessage.convertAndSend("/topic/nine", getDataFrom999Date());
    }

}
